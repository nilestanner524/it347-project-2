var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');

app.get('/', function(req, res){
  res.sendFile(__dirname + '/client/index.html');
});

app.use(express.static(path.join(__dirname, 'client')));

http.listen(3000, function(){
  console.log('listening on *:3000');
});

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('chat message', handleMessage);
  socket.on('disconnect', handleDisconnect);
});

function handleMessage(msg){
  console.log('message: ' + msg);
};

function handleDisconnect(){
  console.log('user disconnected');
};
